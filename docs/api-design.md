# API Design

---

## Accounts

### Create Account

- endpoint path: /api/accounts
- endpoint method: POST
- request shape:
  {
  "first_name": "string",
  "last_name": "string",
  "email": "string",
  "password": "string",
  "location": 0
  }
- response: 200
- response shape (JSON):
  {
  "access_token": "string",
  "token_type": "Bearer",
  "account": {
  "id": 0,
  "first_name": "string",
  "last_name": "string",
  "email": "string",
  "location": 0
  }
  }

### Log In

- endpoint path: /api/accounts/login
- endpoint method: POST
- request shape:
  {
  "username": "string",
  "password": "string"
  }
  response: 200
  Body:
  {
  "access_token": "string",
  "token_type": "Bearer",
  "account": {
  "id": 0,
  "first_name": "string",
  "last_name": "string",
  "email": "string",
  "location": 0
  }
  }

### Log Out

- endpoint path: /api/accounts/logout
- endpoint method: POST
- request shape: none
- response: 200
- response shape (JSON):
  {
  "detail": [
  {
  "loc": [
  "string",
  0
  ],
  "msg": "string",
  "type": "string"
  }
  ]
  }

### Update Account

- endpoint path: /api/accounts/me
- endpoint method: PUT
- request shape:
  {
  "first_name": "string",
  "last_name": "string",
  "email": "string",
  "password": "string",
  "location": 0
  }
- response: 200
- response shape (JSON):
  {
  "id": 0,
  "first_name": "string",
  "last_name": "string",
  "email": "string",
  "location": 0
  }

### Delete Account

- endpoint path: /api/accounts/me
- endpoint method: DELETE
- request shape: none
- response: 200
- response shape (JSON):
  {
  "id": 0,
  "first_name": "string",
  "last_name": "string",
  "email": "string",
  "location": 0
  }

---

## Items

### Create Item

- endpoint path: /items
- endpoint method: POST
- request shape:
  {
  "name": "string",
  "item_type": "string",
  "quantity": 0,
  "purchased_or_prepared": "YYYY-MM-DDTHH:MM:SS.SSSZ",
  "time_of_post": "YYYY-MM-DDTHH:MM:SS.SSSZ",
  "expiration": "YYYY-MM-DDTHH:MM:SS.SSSZ",
  "location": 0,
  "dietary_restriction": [
  "string"
  ],
  "description": "string",
  "pickup_instructions": "string"
  }
- response: 200
- response shape (JSON): none

### Update Item

- endpoint path: /items/{item_id}
- endpoint method: PUT
- request shape:
  {
  "name": "string",
  "item_type": "string",
  "quantity": 0,
  "purchased_or_prepared": "YYYY-MM-DDTHH:MM:SS.SSSZ",
  "time_of_post": "YYYY-MM-DDTHH:MM:SS.SSSZ",
  "expiration": ""YYYY-MM-DDTHH:MM:SS.SSSZ",
  "location": 0,
  "dietary_restriction": [
  "string"
  ],
  "description": "string",
  "pickup_instructions": "string"
  }
- response: 200
- response shape (JSON):
  {
  "id": 0,
  "name": "string",
  "item_type": "string",
  "quantity": 0,
  "purchased_or_prepared": "2023-06-09T21:40:24.981Z",
  "time_of_post": "2023-06-09T21:40:24.981Z",
  "expiration": "2023-06-09T21:40:24.981Z",
  "location": 0,
  "dietary_restriction": [
  "string"
  ],
  "description": "string",
  "pickup_instructions": "string",
  "account_id": 0
  }

### Delete Item

- endpoint path: /items/{item_id}
- endpoint method: DELETE
- request shape: none
- response: 200
- response shape (JSON): none

### Get One Item

- endpoint path: /items/{item_id}
- endpoint method: GET
- request shape: none
- response: 200
- response shape (JSON):
  {
  "id": 0,
  "name": "string",
  "item_type": "string",
  "quantity": 0,
  "purchased_or_prepared": "2023-06-09T21:43:07.859Z",
  "time_of_post": "2023-06-09T21:43:07.859Z",
  "expiration": "2023-06-09T21:43:07.859Z",
  "location": 0,
  "dietary_restriction": [
  "string"
  ],
  "description": "string",
  "pickup_instructions": "string",
  "account_id": 0
  }

### Get All Items

- endpoint path: /items
- endpoint method: GET
- request shape: none
- response: 200
- response shape (JSON):
  [
  {
  "id": 0,
  "name": "string",
  "item_type": "string",
  "quantity": 0,
  "purchased_or_prepared": "2023-06-09T21:43:07.859Z",
  "time_of_post": "2023-06-09T21:43:07.859Z",
  "expiration": "2023-06-09T21:43:07.859Z",
  "location": 0,
  "dietary_restriction": [
  "string"
  ],
  "description": "string",
  "pickup_instructions": "string",
  "account_id": 0
  },
  {
  "id": 1,
  "name": "string",
  "item_type": "string",
  "quantity": 0,
  "purchased_or_prepared": "2023-06-09T21:43:07.859Z",
  "time_of_post": "2023-06-09T21:43:07.859Z",
  "expiration": "2023-06-09T21:43:07.859Z",
  "location": 0,
  "dietary_restriction": [
  "string"
  ],
  "description": "string",
  "pickup_instructions": "string",
  "account_id": 0
  }
  ]
