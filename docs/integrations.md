# Integrations

---

The application will need to get the following kinds of data from third-party sources:

- Currently using Pexels API to generate item images (image hosting is a stretch goal)

- Using OpenWeather API to translate ZIP codes into Lat/Lon coordinates for distance calculation, as well as map on Item Detail to show the approximate location of an item.
